package com.example.BookProjectNewTaofik.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {

	@Query(value = "SELECT u FROM Author u")
	List<Author> findAllAuthor();
	
	@Query("SELECT u FROM Author u WHERE u.id = ?1")
	Author findUserById(Long id);
	
	@Modifying
	@Query("DELETE FROM Author u WHERE u.id = ?1")
	void deleteUserById(Long id);
	
	@Modifying
	@Query(value = "Insert Into Author(age, country, first_name, gender, last_name, string_rating) values (:age, :country, :firstName, :gender, :lastName, :rating)", nativeQuery = true)
	void insertAuthor(@Param("age") Integer age,
						@Param("country") String country,
						@Param("firstName") String firstName,
						@Param("gender") String gender,
						@Param("lastName") String lastName,
						@Param("rating") String rating);
	
	@Modifying
	@Query(value = "update Author set age = ?, country = ?, first_name = ?, gender = ?, last_name = ?, string_rating = ? where author_id = ?", nativeQuery = true)
	void updateAuthor(Integer age, String country, String fistName, String gender, String lastName, String rating, Long authorId);
}
