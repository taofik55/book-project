package com.example.BookProjectNewTaofik.repositories;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.Paper;

@Repository
public interface PaperRepository extends JpaRepository<Paper, Long> {

	@Query(value = "SELECT u FROM Paper u")
	List<Paper> findAllPaper();
	
	@Query("SELECT u FROM Paper u WHERE u.id = ?1")
	Paper findUserById(Long id);
	
	@Modifying
	@Query("DELETE FROM Paper u WHERE u.id = ?1")
	void deleteUserById(Long id);
	
	@Modifying
    @Query(value = "INSERT INTO Paper (quality_name, price) VALUES (:qualityName, :paperPrice)", nativeQuery = true)
    void insertPaper(@Param("qualityName") String qualityName,
    				@Param("paperPrice") BigDecimal paperPrice);
	
	@Modifying
	@Query(value = "update Paper set quality_name = ?, price = ? where paper_id = ?", nativeQuery = true)
	void updatePaper(String qualityName, BigDecimal paperPrice, Long paperId); 
}
