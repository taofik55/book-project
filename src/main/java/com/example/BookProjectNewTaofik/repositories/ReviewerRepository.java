package com.example.BookProjectNewTaofik.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.Reviewer;

@Repository
public interface ReviewerRepository extends JpaRepository<Reviewer, Long> {
	@Query(value = "SELECT u FROM Reviewer u")
	List<Reviewer> findAllReviewer();
	
	@Query("SELECT u FROM Reviewer u WHERE u.id = ?1")
	Reviewer findReviewerById(Long id);
	
	@Modifying
	@Query("DELETE FROM Reviewer u WHERE u.id = ?1")
	void deleteUserById(Long id);
	
	@Modifying
	@Query(value = "Insert Into Reviewer(reviewer_name, country, verified) values (:reviewerName, :country, :verified)", nativeQuery = true)
	void insertReviewer(@Param("reviewerName") String reviewerName,
						@Param("country") String country,
						@Param("verified") boolean verified);
	
	@Modifying
	@Query(value = "update Reviewer set reviewer_name = ?, country = ?, verified = ? where reviewer_id = ?", nativeQuery = true)
	void updateReviewer(String reviewerName,
						String country,
						boolean verified,
						Long reviewerId);
}
