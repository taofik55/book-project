package com.example.BookProjectNewTaofik.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.Author;
import com.example.BookProjectNewTaofik.models.Book;
import com.example.BookProjectNewTaofik.models.Publisher;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

	@Query("SELECT u FROM Book u")
	List<Book> findAllBook();
	
	@Query("SELECT u FROM Book u WHERE u.id = ?1")
	Book findBookById(Long id);
	
	@Modifying
	@Query("DELETE FROM Book u WHERE u.id = ?1")
	void deleteBookById(Long id);
	
	@Modifying
	@Query(value = "INSERT INTO Book (price, release_date, title, author_id, publisher_id) VALUES (:price, :releaseDate, :title, :authorId, :publisherId)", nativeQuery = true)
	void insertBook(@Param("price") BigDecimal price,
					@Param("releaseDate") Date releaseDate,
					@Param("title") String title,
					@Param("authorId") Author authorId,
					@Param("publisherId") Publisher publisherId);
	
	@Modifying
    @Query(value = "update Book set title = ?, release_date = ?, author_id = ?, publisher_id = ?, price = ? where book_id = ?", nativeQuery = true)
    int updateBook(String title, Date releaseDate, Author authors, Publisher publishers, BigDecimal price, Long bookId);
}
