package com.example.BookProjectNewTaofik.repositories;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.OrderBookDetails;
import com.example.BookProjectNewTaofik.models.OrderDetailsKey;

@Repository
public interface OrderBookDetailsRepository extends JpaRepository<OrderBookDetails, OrderDetailsKey> {

	@Query(value = "SELECT u FROM OrderBookDetails u")
	List<OrderBookDetails> findOrderDetails();
	
	@Query("SELECT u FROM OrderBookDetails u WHERE u.id = ?1")
	OrderBookDetails findOrderDetailsId(OrderDetailsKey id);
	
	@Modifying
	@Query("DELETE FROM OrderBookDetails u WHERE u.id = ?1")
	void deleteOrderDetails(OrderDetailsKey id);
	
	@Modifying
	@Query(value = "INSERT INTO Order_book_details (book_id, order_id, quantity, discount, tax) VALUES (:bookId, :orderId, :quantity, :discount, :tax)", nativeQuery = true)
	void insertOrderDetails(@Param("bookId") Long book,
							@Param("orderId") Long order,
							@Param("quantity") int quantity,
							@Param("discount") BigDecimal discount,
							@Param("tax") BigDecimal tax);
	
	@Modifying
	@Query(value = "update Publisher set quantity = :quantity, discount = :discount, tax = :tax where book_id = :bookId and order_id = :orderId", nativeQuery = true)
	void updatePublisher(@Param("quantity") int quantity,
						@Param("discount") BigDecimal discount,
						@Param("tax") BigDecimal tax,
						@Param("bookId") Long book,
						@Param("orderId") Long order);
}
