package com.example.BookProjectNewTaofik.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.Book;
import com.example.BookProjectNewTaofik.models.BookRating;
import com.example.BookProjectNewTaofik.models.Reviewer;

@Repository
public interface BookRatingRepository extends JpaRepository<BookRating, Long> {
	@Query(value = "SELECT u FROM BookRating u")
	List<BookRating> findAllBookRating();
	
	@Query("SELECT u FROM BookRating u WHERE u.id = ?1")
	BookRating findUserById(Long id);
	
	@Modifying
	@Query("DELETE FROM BookRating u WHERE u.id = ?1")
	void deleteUserById(Long id);
	
	@Modifying
	@Query(value = "Insert Into book_rating(rating_name, rating_rate, rating_score, book_id, reviewer_id) values (:ratingName, :ratingRate, :ratingScore, :bookId, :reviewerId)", nativeQuery = true)
	void insertBookRating(@Param("ratingName") String ratingName,
						@Param("ratingRate") double ratingRate,
						@Param("ratingScore") int ratingScore,
						@Param("bookId") Book bookId,
						@Param("reviewerId") Reviewer reviewerId);
	
	@Modifying
	@Query(value = "update book_rating set rating_name = ?, rating_rate = ?, rating_score = ?, book_id = ?, reviewer_id = ? where book_rating_id = ?", nativeQuery = true)
	void updateBookRating(String ratingName,
						double ratingRate,
						int ratingScore,
						Book bookId,
						Reviewer reviewerId,
						Long bookRatingId);
}
