package com.example.BookProjectNewTaofik.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.Paper;
import com.example.BookProjectNewTaofik.models.Publisher;

@Repository
public interface PublisherRepository extends JpaRepository<Publisher, Long> {
	@Query(value = "SELECT u FROM Publisher u")
	List<Publisher> findAllPublisher();
	
	@Query("SELECT u FROM Publisher u WHERE u.id = ?1")
	Publisher findUserById(Long id);
	
	@Modifying
	@Query("DELETE FROM Publisher u WHERE u.id = ?1")
	void deleteUserById(Long id);
	
	@Modifying
	@Query(value = "Insert Into Publisher(company_name, country, paper_id) values (:companyName, :country, :paperId)", nativeQuery = true)
	void insertPublisher(@Param("companyName") String companyName,
						@Param("country") String country,
						@Param("paperId") Paper paper);
	
	@Modifying
	@Query(value = "update Publisher set company_name = ?, country = ?, paper_id = ? where publisher_id = ?", nativeQuery = true)
	void updatePublisher(String companyName,
						String country,
						Paper paperId,
						Long publisherId);
}
