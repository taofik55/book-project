package com.example.BookProjectNewTaofik.repositories;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.Customer;
import com.example.BookProjectNewTaofik.models.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
	@Query(value = "SELECT u FROM Order u")
	List<Order> findAllOrder();
	
	@Query("SELECT u FROM Order u WHERE u.id = ?1")
	Order findUserById(Long id);
	
	@Modifying
	@Query("DELETE FROM Order u WHERE u.id = ?1")
	void deleteUserById(Long id);
	
	@Modifying
	@Query(value = "Insert Into Orders(order_date, total_order, customer_id) values (:orderDate, :totalOrder, :customerId)", nativeQuery = true)
	void insertOrder(@Param("orderDate") Date orderDate,
						@Param("totalOrder") BigDecimal totalOrder,
						@Param("customerId") Customer customer);
	
	@Modifying
	@Query(value = "update Orders set order_date = :orderDate, total_order = :totalOrder, customer_id = :customerId where order_id = :orderId", nativeQuery = true)
	void updateOrder(@Param("orderDate") Date orderDate,
			@Param("totalOrder") BigDecimal totalOrder,
			@Param("customerId") Customer customer,
			@Param("orderId") Long orderId);
}
