package com.example.BookProjectNewTaofik.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.BookProjectNewTaofik.models.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>{

}
