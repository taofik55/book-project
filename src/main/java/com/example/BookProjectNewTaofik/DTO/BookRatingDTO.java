package com.example.BookProjectNewTaofik.DTO;

public class BookRatingDTO {
	private Long bookRatingId;
	private BookDTO book;
	private ReviewerDTO reviewer;
	private int ratingScore;
	public BookRatingDTO(Long bookRatingId, BookDTO book, ReviewerDTO reviewer, int ratingScore) {
		super();
		this.bookRatingId = bookRatingId;
		this.book = book;
		this.reviewer = reviewer;
		this.ratingScore = ratingScore;
	}
	public BookRatingDTO() {
		super();
	}
	public Long getBookRatingId() {
		return bookRatingId;
	}
	public void setBookRatingId(Long bookRatingId) {
		this.bookRatingId = bookRatingId;
	}
	public BookDTO getBook() {
		return book;
	}
	public void setBook(BookDTO book) {
		this.book = book;
	}
	public ReviewerDTO getReviewer() {
		return reviewer;
	}
	public void setReviewer(ReviewerDTO reviewer) {
		this.reviewer = reviewer;
	}
	public int getRatingScore() {
		return ratingScore;
	}
	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}
	
	
}
