package com.example.BookProjectNewTaofik.DTO;

public class ReviewerDTO {
	
	private Long reviewerId;
	private String reviewerName;
	private String country;
	private Boolean verified;
	public ReviewerDTO(Long reviewerId, String reviewerName, String country, Boolean verified) {
		super();
		this.reviewerId = reviewerId;
		this.reviewerName = reviewerName;
		this.country = country;
		this.verified = verified;
	}
	public ReviewerDTO() {
		super();
	}
	public Long getReviewerId() {
		return reviewerId;
	}
	public void setReviewerId(Long reviewId) {
		this.reviewerId = reviewId;
	}
	public String getReviewerName() {
		return reviewerName;
	}
	public void setReviewerName(String reviewName) {
		this.reviewerName = reviewName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Boolean getVerified() {
		return verified;
	}
	public void setVerified(Boolean verified) {
		this.verified = verified;
	}
	
	
}
