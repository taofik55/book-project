package com.example.BookProjectNewTaofik.DTO;

import java.math.BigDecimal;

import com.example.BookProjectNewTaofik.models.OrderDetailsKey;

public class OrderBookDetailsDTO {
	private OrderDetailsKey id;
	private int quantity;
	private BigDecimal discount;
	private BigDecimal tax;
	private BookDTO book;
	private OrderDTO order;
	
	public OrderBookDetailsDTO(OrderDetailsKey id, int quantity, BigDecimal discount,
			BigDecimal tax, BookDTO book, OrderDTO order) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.discount = discount;
		this.tax = tax;
		this.book = book;
		this.order = order;
	}
	public OrderBookDetailsDTO() {
		super();
	}
	public OrderDetailsKey getId() {
		return id;
	}
	public void setId(OrderDetailsKey id) {
		this.id = id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	public BigDecimal getTax() {
		return tax;
	}
	public void setTax(BigDecimal tax) {
		this.tax = tax;
	}
	public BookDTO getBook() {
		return book;
	}
	public void setBook(BookDTO book) {
		this.book = book;
	}
	public OrderDTO getOrder() {
		return order;
	}
	public void setOrder(OrderDTO order) {
		this.order = order;
	}
	
	
}
