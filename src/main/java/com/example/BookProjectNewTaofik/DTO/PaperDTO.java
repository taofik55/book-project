package com.example.BookProjectNewTaofik.DTO;

import java.math.BigDecimal;

public class PaperDTO {
	private Long paperId;
	private String qualityName;
	private BigDecimal paperPrice;
	public PaperDTO(Long paperId, String qualityName, BigDecimal paperPrice) {
		super();
		this.paperId = paperId;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
	}
	public PaperDTO() {
		super();
	}
	public Long getPaperId() {
		return paperId;
	}
	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}
	public String getQualityName() {
		return qualityName;
	}
	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	public BigDecimal getPaperPrice() {
		return paperPrice;
	}
	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}
	
	
}
