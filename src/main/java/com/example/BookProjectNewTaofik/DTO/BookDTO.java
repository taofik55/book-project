package com.example.BookProjectNewTaofik.DTO;

import java.math.BigDecimal;
import java.util.Date;

public class BookDTO {
	private Long bookId;
	private String title;
	private Date releaseDate;
	private AuthorDTO author;
	private PublisherDTO publisher;
	private BigDecimal price;
	public BookDTO(Long bookId, String title, Date releaseDate, AuthorDTO author, PublisherDTO publisher,
			BigDecimal price) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
	}
	public BookDTO() {
		super();
	}
	public Long getBookId() {
		return bookId;
	}
	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public AuthorDTO getAuthor() {
		return author;
	}
	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}
	public PublisherDTO getPublisher() {
		return publisher;
	}
	public void setPublisher(PublisherDTO publisher) {
		this.publisher = publisher;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	
}
