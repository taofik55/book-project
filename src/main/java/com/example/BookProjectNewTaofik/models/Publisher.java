package com.example.BookProjectNewTaofik.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "publisher")
public class Publisher implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2682858373295967057L;


	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_publisher_id_publisher_seq")
	@SequenceGenerator(name = "generator_publisher_id_publisher_seq", sequenceName = "publisher_id_publisher_seq", schema = "public", allocationSize = 1)
	@Column(name = "publisher_id", unique = true, nullable = false)
	private Long publisherId;
	
	@Column(name = "companyName", nullable = false)
	private String companyName;
	
	@Column(name = "country")
	private String country;
	
	@ManyToOne
	@JoinColumn(name = "paper_id")
	private Paper paper;

	@OneToMany(mappedBy = "publisher")
	private Set<Book> books;
	
	public Publisher(Long publisherId, String companyName, String country, Paper paper,
			Set<Book> books) {
		super();
		this.publisherId = publisherId;
		this.companyName = companyName;
		this.country = country;
		this.paper = paper;
		this.books = books;
	}

	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
	}

	public Publisher() {
		super();
	}

	public Long getPublisherId() {
		return publisherId;
	}

	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Paper getPaper() {
		return paper;
	}

	public void setPaper(Paper paper) {
		this.paper = paper;
	}
	
	
}
