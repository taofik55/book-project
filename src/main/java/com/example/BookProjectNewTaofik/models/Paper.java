package com.example.BookProjectNewTaofik.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "paper")
public class Paper implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4463649365705378951L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_paper_id_paper_seq")
	@SequenceGenerator(name = "generator_paper_id_paper_seq", sequenceName = "paper_id_paper_seq", schema = "public", allocationSize = 1)
	@Column(name = "paper_id", unique = true, nullable = false, insertable = true)
	private Long paperId;
	
	@Column(name = "quality_name", nullable = false)
	private String qualityName;
	
	@Column(name = "price", nullable = false)
	private BigDecimal paperPrice;
	
	@OneToMany(mappedBy = "paper")
	private Set<Publisher> publishers;

	public Paper(Long paperId, String qualityName, BigDecimal paperPrice, Set<Publisher> publishers) {
		super();
		this.paperId = paperId;
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		this.publishers = publishers;
	}

	public Paper() {
		super();
	}

	public Long getPaperId() {
		return paperId;
	}

	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}

	public Set<Publisher> getPublishers() {
		return publishers;
	}

	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
	}

}
