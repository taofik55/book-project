package com.example.BookProjectNewTaofik.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Book")
public class Book implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_book_id_book_seq")
	@SequenceGenerator(name = "generator_book_id_book_seq", sequenceName = "book_id_book_seq", schema = "public", allocationSize = 1)
	@Column(name = "book_id", unique = true, nullable = false)
	private Long bookId;
	
	@Column(name = "title", nullable = false)
	private String title;
	
	@Column(name = "releaseDate", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date releaseDate;
	
	@ManyToOne
	@JoinColumn(name = "author_id")
	private Author author;
	
	@ManyToOne
	@JoinColumn(name = "publisher_id")
	private Publisher publisher;
	
	@Column(name = "price", nullable = false)
	private BigDecimal price;
	
	@OneToMany(mappedBy = "book")
	private Set<BookRating> bookRatings;
	
	@OneToMany(mappedBy = "book")
	private Set<OrderBookDetails> orderDetails;

	public Book(Long bookId, String title, Date releaseDate, Author author, Publisher publisher,
			BigDecimal price, Set<BookRating> bookRatings, Set<OrderBookDetails> orderDetails) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.author = author;
		this.publisher = publisher;
		this.price = price;
		this.bookRatings = bookRatings;
		this.orderDetails = orderDetails;
	}

	public Set<OrderBookDetails> getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(Set<OrderBookDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Set<BookRating> getBookRatings() {
		return bookRatings;
	}

	public void setBookRatings(Set<BookRating> bookRatings) {
		this.bookRatings = bookRatings;
	}

	public Book() {
		super();
	}

	public Long getBookId() {
		return bookId;
	}

	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	
}
