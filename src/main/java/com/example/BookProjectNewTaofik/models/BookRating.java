package com.example.BookProjectNewTaofik.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "book_rating")
public class BookRating implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1828565471441951792L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_book_rating_id_book_rating_seq")
	@SequenceGenerator(name = "generator_book_rating_id_book_rating_seq", sequenceName = "book_rating_id_book_rating_seq", schema = "public", allocationSize = 1)
	@Column(name = "book_rating_id", unique = true, nullable = false)
	private Long bookRatingId;
	
	@Column(name = "ratingName", nullable = false)
	private String ratingName;
	
	@Column(name = "ratingRate", nullable = false)
	private Double ratingRate;

	@ManyToOne
	@JoinColumn(name = "book_id")
	private Book book;
	
	@ManyToOne
	@JoinColumn(name = "reviewer_id")
	private Reviewer reviewer;
	
	@Column(name = "rating_score", nullable = false)
	private int ratingScore;

	public BookRating(Long bookRatingId, String ratingName, Double ratingRate, Book book, Reviewer reviewer,
			int ratingScore) {
		super();
		this.bookRatingId = bookRatingId;
		this.ratingName = ratingName;
		this.ratingRate = ratingRate;
		this.book = book;
		this.reviewer = reviewer;
		this.ratingScore = ratingScore;
	}

	public BookRating() {
		super();
	}

	public Long getBookRatingId() {
		return bookRatingId;
	}

	public void setBookRatingId(Long bookRatingId) {
		this.bookRatingId = bookRatingId;
	}

	public String getRatingName() {
		return ratingName;
	}

	public void setRatingName(String ratingName) {
		this.ratingName = ratingName;
	}

	public Double getRatingRate() {
		return ratingRate;
	}

	public void setRatingRate(Double ratingRate) {
		this.ratingRate = ratingRate;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public Reviewer getReviewer() {
		return reviewer;
	}

	public void setReviewer(Reviewer reviewer) {
		this.reviewer = reviewer;
	}

	public int getRatingScore() {
		return ratingScore;
	}

	public void setRatingScore(int ratingScore) {
		this.ratingScore = ratingScore;
	}
	
	
}
