package com.example.BookProjectNewTaofik.models;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "reviewer")
public class Reviewer implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7817364129060429499L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_reviewer_id_reviewer_seq")
	@SequenceGenerator(name = "generator_reviewer_id_reviewer_seq", sequenceName = "reviewer_id_reviewer_seq", schema = "public", allocationSize = 1)
	@Column(name = "reviewer_id", unique = true, nullable = false)
	private Long reviewerId;
	
	@Column(name = "reviewer_name", nullable = false)
	private String reviewerName;
	
	@Column(name = "country", nullable = false)
	private String country;
	
	@Column(name = "verified", nullable = false)
	private boolean verified;
	
	@OneToMany(mappedBy = "reviewer")
	Set<BookRating> bookRatings;

	public Reviewer(Long reviewerId, String reviewerName, String country, boolean verified,
			Set<BookRating> bookRatings) {
		super();
		this.reviewerId = reviewerId;
		this.reviewerName = reviewerName;
		this.country = country;
		this.verified = verified;
		this.bookRatings = bookRatings;
	}

	public Reviewer() {
		super();
	}

	public Long getReviewerId() {
		return reviewerId;
	}

	public void setReviewerId(Long reviewerId) {
		this.reviewerId = reviewerId;
	}

	public String getReviewerName() {
		return reviewerName;
	}

	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public Set<BookRating> getBookRatings() {
		return bookRatings;
	}

	public void setBookRatings(Set<BookRating> bookRatings) {
		this.bookRatings = bookRatings;
	}
	
	
	
}
