package com.example.BookProjectNewTaofik.ols;

import com.io.iona.core.data.annotations.OptionListKey;

public class ReviewerOLO {
	@OptionListKey
	private Long reviewerId;
	private String reviewerName;
	public Long getReviewerId() {
		return reviewerId;
	}
	public void setReviewerId(Long reviewerId) {
		this.reviewerId = reviewerId;
	}
	public String getReviewerName() {
		return reviewerName;
	}
	public void setReviewerName(String reviewerName) {
		this.reviewerName = reviewerName;
	}
	
}
