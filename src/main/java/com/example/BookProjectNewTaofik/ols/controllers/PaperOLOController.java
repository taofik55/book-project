package com.example.BookProjectNewTaofik.ols.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.models.Paper;
import com.example.BookProjectNewTaofik.ols.PaperOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;

@RestController
@RequestMapping("/ol/paper")
public class PaperOLOController extends HibernateOptionListController<Paper, PaperOLO> {

}
