package com.example.BookProjectNewTaofik.ols.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.models.Reviewer;
import com.example.BookProjectNewTaofik.ols.ReviewerOLO;
import com.io.iona.springboot.controllers.HibernateOptionListController;

@RestController
@RequestMapping("/ol/reviewer")
public class ReviewerOLOController extends HibernateOptionListController<Reviewer, ReviewerOLO> {

}
