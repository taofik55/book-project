package com.example.BookProjectNewTaofik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookProjectNewTaofikApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookProjectNewTaofikApplication.class, args);
	}

}
