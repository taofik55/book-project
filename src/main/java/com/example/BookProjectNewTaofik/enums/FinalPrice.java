package com.example.BookProjectNewTaofik.enums;

import java.math.BigDecimal;

public interface FinalPrice {
	BigDecimal discountRate =  new BigDecimal(0.1);
	double taxRate = 0.05;
	double costRate = 1.5;
}
