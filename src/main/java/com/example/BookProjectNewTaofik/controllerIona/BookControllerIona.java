package com.example.BookProjectNewTaofik.controllerIona;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.BookDTO;
import com.example.BookProjectNewTaofik.models.Author;
import com.example.BookProjectNewTaofik.models.Book;
import com.example.BookProjectNewTaofik.repositories.AuthorRepository;
import com.example.BookProjectNewTaofik.repositories.BookRepository;
import com.io.iona.springboot.actionflows.custom.CustomOnInsert;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/api/iona/book")
public class BookControllerIona extends HibernateCRUDController<Book, BookDTO> implements CustomOnInsert<Book, BookDTO>{ 
	@Autowired
	BookRepository bookRepo;
	
	@Autowired
	AuthorRepository authorRepo;
	
	@Override
	public Book onInsert(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Book, BookDTO> dataSource) throws Exception {
        Book book = dataSource.getDataModel();
        authorRepo.findById(book.getAuthor().getAuthorId());
        if(authorRepo.findById(book.getAuthor().getAuthorId()).isPresent() == false) {
        	Author authorEntity = new Author();
        	authorEntity.setAge(1);
        	authorEntity.setCountry("Indonesia");
        	authorEntity.setFirstName("New Name");
        	authorEntity.setLastName("New Name");
        	authorEntity.setGender("Gender");
        	authorEntity.setRating("Good");
        	System.out.println(authorEntity.getAuthorId());
        	authorRepo.save(authorEntity);
        }
        bookRepo.save(book);
		return dataSource.getDataModel();
	}

}
