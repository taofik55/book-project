package com.example.BookProjectNewTaofik.controllerIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.AuthorDTO;
import com.example.BookProjectNewTaofik.models.Author;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/author")
public class AuthorControllerIona extends HibernateCRUDController<Author, AuthorDTO> {

}
