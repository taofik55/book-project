package com.example.BookProjectNewTaofik.controllerIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.OrderBookDetailsDTO;
import com.example.BookProjectNewTaofik.models.OrderBookDetails;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/orderbookdetails")
public class OrderBookDetailsControllerIona extends HibernateCRUDController<OrderBookDetails, OrderBookDetailsDTO> {

}
