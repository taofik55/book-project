package com.example.BookProjectNewTaofik.controllerIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.ReviewerDTO;
import com.example.BookProjectNewTaofik.models.Reviewer;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/reviewer")
public class ReviewerControllerIona extends HibernateCRUDController<Reviewer, ReviewerDTO> {

}
