package com.example.BookProjectNewTaofik.controllerIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.CustomerDTO;
import com.example.BookProjectNewTaofik.models.Customer;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/customer")
public class CustomerControllerIona extends HibernateCRUDController<Customer, CustomerDTO>{

}
