package com.example.BookProjectNewTaofik.controllerIona;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.OrderDTO;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.models.Order;
import com.example.BookProjectNewTaofik.repositories.CustomerRepository;
import com.example.BookProjectNewTaofik.models.Customer;
import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/api/iona/order")
public class OrderControllerIona extends HibernateCRUDController<Order, OrderDTO> implements CustomAfterReadAll<Order, OrderDTO> {

	ModelMapper mapper = new ModelMapper();
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Override
    public List<OrderDTO> afterReadAll(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Order, OrderDTO> dataSource, DefaultPagingParameter defaultPagingParameter) throws Exception {
        List<Order> orders = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);
        List<OrderDTO> orderDto = new ArrayList<>();

        for(Order p: orders){
            OrderDTO dto = mapper.map(p, OrderDTO.class);
            Customer customer = customerRepository.findById(p.getCustomer().getCustomerId()).orElseThrow(() -> new ResourceNotFoundException("Customer", "id", p.getCustomer().getCustomerId()));
            dto.setCustomerName(customer.getCustomerName());
            dto.setAddress(customer.getAddress());
            dto.setCountry(customer.getCountry());
            dto.setPhoneNumber(customer.getPhoneNumber());
            dto.setEmail(customer.getEmail());
            dto.setPostalCode(customer.getPostalCode());
            orderDto.add(dto);
        }
        return orderDto;
    }

}
