package com.example.BookProjectNewTaofik.controllerIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.PaperDTO;
import com.example.BookProjectNewTaofik.models.Paper;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/paper")
public class PaperControllerIona extends HibernateCRUDController<Paper, PaperDTO>{

	
}
