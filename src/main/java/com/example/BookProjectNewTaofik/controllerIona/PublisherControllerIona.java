package com.example.BookProjectNewTaofik.controllerIona;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.exceptions.*;
import com.example.BookProjectNewTaofik.DTO.PublisherDTO;
import com.example.BookProjectNewTaofik.models.Paper;
import com.example.BookProjectNewTaofik.models.Publisher;
import com.example.BookProjectNewTaofik.repositories.PaperRepository;
import com.example.BookProjectNewTaofik.repositories.PublisherRepository;
import com.io.iona.core.enums.ActionFlow;
import com.io.iona.implementations.pagination.DefaultPagingParameter;
import com.io.iona.springboot.actionflows.custom.CustomAfterReadAll;
import com.io.iona.springboot.actionflows.custom.CustomBeforeInsert;
import com.io.iona.springboot.actionflows.custom.CustomOnInsert;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;

@RestController
@RequestMapping("/api/iona/publisher")
public class PublisherControllerIona extends HibernateCRUDController<Publisher, PublisherDTO>
	implements CustomOnInsert<Publisher, PublisherDTO>,
				CustomAfterReadAll<Publisher, PublisherDTO>,
				CustomBeforeInsert<Publisher, PublisherDTO>{
	
	@Autowired
	PaperRepository paperRepository;
	
	@Autowired
	PublisherRepository publisherRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@Override
	public Publisher onInsert(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Publisher, PublisherDTO> dataSource)
			throws Exception {
		Publisher publisher = dataSource.getDataModel();
		paperRepository.findById(publisher.getPaper().getPaperId());			
		if (paperRepository.findById(publisher.getPaper().getPaperId()).isPresent() == false) {
			Paper paper = new Paper();
			paper.setPaperId(publisher.getPaper().getPaperId());
			paper.setPaperPrice(new BigDecimal(1));
			paper.setQualityName("Kualiti Baru");
			paperRepository.save(paper);
		}
		publisherRepository.save(publisher);
		return dataSource.getDataModel();
	}

    @Override
    public List<PublisherDTO> afterReadAll(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Publisher, PublisherDTO> dataSource, DefaultPagingParameter defaultPagingParameter) throws Exception {
        List<Publisher> publishers = dataSource.getResult(ActionFlow.ON_READ_ALL_ITEMS, List.class);
        List<PublisherDTO> publisherDto = new ArrayList<>();

        for(Publisher p: publishers){
            PublisherDTO dto = mapper.map(p, PublisherDTO.class);
            Paper paper = paperRepository.findById(p.getPaper().getPaperId()).orElseThrow(() -> new ResourceNotFoundException("Paper", "id", p.getPaper().getPaperId()));
            dto.setQualityName(paper.getQualityName());
            publisherDto.add(dto);
        }
        return publisherDto;
    }

	@Override
	public void beforeInsert(HibernateDataUtility hibernateDataUtility, HibernateDataSource<Publisher, PublisherDTO> dataSource)
			throws Exception {
		Publisher publisher = dataSource.getDataModel();
		PublisherDTO dto = mapper.map(publisher, PublisherDTO.class);
		paperRepository.findById(dto.getPaperId()).orElseThrow(()-> new ResourceNotFoundException("Paper", "id", dto.getPaperId()));
		
	}

}
