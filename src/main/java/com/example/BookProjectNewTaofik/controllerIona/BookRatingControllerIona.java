package com.example.BookProjectNewTaofik.controllerIona;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.BookRatingDTO;
import com.example.BookProjectNewTaofik.models.BookRating;
import com.io.iona.springboot.controllers.HibernateCRUDController;

@RestController
@RequestMapping("/api/iona/bookrating")
public class BookRatingControllerIona extends HibernateCRUDController<BookRating, BookRatingDTO> {

}
