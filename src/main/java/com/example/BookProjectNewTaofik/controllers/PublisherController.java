package com.example.BookProjectNewTaofik.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.PublisherDTO;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.models.Publisher;
import com.example.BookProjectNewTaofik.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/publisher")
public class PublisherController {
	
	@Autowired
	PublisherRepository publisherRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<Publisher> getAllPublishers() {
        return publisherRepository.findAll();
    }
	
	@PostMapping("/create")
	public Publisher createPublisher(@RequestBody Publisher publisher){
		return publisherRepository.save(publisher);
	}
	
	@PutMapping("/update/{id}")
	public Publisher updatePublisher(@PathVariable(value = "id") Long publisherId,
            @Valid @RequestBody Publisher publisherDetails) {

		Publisher publisher = publisherRepository.findById(publisherId)
		 	.orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));

		publisher.setCompanyName(publisherDetails.getCompanyName());
		publisher.setCountry(publisherDetails.getCountry());
		publisher.setPaper(publisherDetails.getPaper());

		Publisher updatedPublisher = publisherRepository.save(publisher);
		return updatedPublisher;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deletePublisher(@PathVariable(value = "id") Long publisherId) {
       Publisher publisher = publisherRepository.findById(publisherId)
                .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));

       publisherRepository.delete(publisher);

       return ResponseEntity.ok().build();
	}
	 
	@GetMapping("/get/{id}")
	public Publisher getPublisherById(@PathVariable(value = "id") Long publisherId) {
        return publisherRepository.findById(publisherId)
                .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));
    }
	
	@Transactional
	@PostMapping("/createDto")
	public Map<String, Object> createPublisherDto(@RequestBody Publisher publisher) {
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisherEntity = mapper.map(publisher, Publisher.class);
		publisherRepository.save(publisherEntity);
		publisher.setPublisherId(publisherEntity.getPublisherId());
		result.put("Data", publisher);
		return result;
		
	}
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findPublishersDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Publisher> listPublisherEntity = publisherRepository.findAllPublisher();
		List<PublisherDTO> listPublisherDto = new ArrayList<PublisherDTO>();
		
		for(Publisher publisherEntity : listPublisherEntity) {
			PublisherDTO map = mapper.map(publisherEntity, PublisherDTO.class);
			listPublisherDto.add(map);
		}
		
		result.put("Message", "Read All Publisher Success");
		result.put("Data", listPublisherDto);
		result.put("totalItem", listPublisherDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{id}")
	public  Map<String, Object> getPublisherByIdDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Publisher publisherEntity = publisherRepository.findUserById(id);
		
		PublisherDTO map = mapper.map(publisherEntity, PublisherDTO.class);
		
		result.put("Status", "Success");
		result.put("Message", "Read Publisher by Id Success");
		result.put("Data", map);
		
		return result;
	}
	
	@Transactional
	@PutMapping("/updateDto/{id}")
	public  Map<String, Object> updatePublisherDto(@RequestBody Publisher publisher, @PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Publisher publisherEntity = publisherRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", id));
		
		publisherEntity = mapper.map(publisher, Publisher.class);
		publisherEntity.setPublisherId(id);
		publisherRepository.save(publisherEntity);
		
		result.put("message", "Update Publisher Success");
		result.put("data", publisher);
		
		return result;
	}
	
	@Transactional
	@DeleteMapping("/deleteDto/{id}")
	public  Map<String, Object> deletePublisherDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Publisher publisherEntity = publisherRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", id));
		
		PublisherDTO map = mapper.map(publisherEntity, PublisherDTO.class);
		publisherRepository.deleteUserById(id);
		
		result.put("message", "Delete Publisher Success");
		result.put("data", map);
		
		return result;
	}
	
}
