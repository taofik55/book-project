package com.example.BookProjectNewTaofik.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.PaperDTO;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.models.Paper;
import com.example.BookProjectNewTaofik.repositories.PaperRepository;

@RestController
@RequestMapping("/api/paper")
public class PaperController {
	
	@Autowired
	PaperRepository paperRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<Paper> getAllPapers() {
        return paperRepository.findAllPaper();
    }
	
	@PostMapping("/create")
	public Paper createPaper(@RequestBody Paper paper){
		return paperRepository.save(paper);
	}
	
	@PutMapping("/update/{id}")
	public Paper updatePaper(@PathVariable(value = "id") Long paperId,
            @Valid @RequestBody Paper paperDetails) {

		Paper paper = paperRepository.findById(paperId)
		 	.orElseThrow(() -> new ResourceNotFoundException("Paper", "id", paperId));

		paper.setPaperPrice(paperDetails.getPaperPrice());
		paper.setQualityName(paperDetails.getQualityName());

		Paper updatedPaper = paperRepository.save(paper);
		return updatedPaper;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deletePaper(@PathVariable(value = "id") Long paperId) {
       Paper paper = paperRepository.findUserById(paperId);

       paperRepository.delete(paper);

       return ResponseEntity.ok().build();
	}
	 
	@GetMapping("/get/{id}")
	public Paper getPaperById(@PathVariable(value = "id") Long paperId) {
        return paperRepository.findUserById(paperId);
    }
	
	@Transactional
	@PostMapping("/createDto")
	public Map<String, Object> createPaperDto(@RequestBody PaperDTO paper) {
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = mapper.map(paper, Paper.class);
		paperRepository.insertPaper(paperEntity.getQualityName(), paperEntity.getPaperPrice());
		paper.setPaperId(paperEntity.getPaperId());
		result.put("Data", paper);
		return result;
		
	}
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findPapersDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Paper> listPaperEntity = paperRepository.findAllPaper();
		List<PaperDTO> listPaperDto = new ArrayList<PaperDTO>();
		
		for(Paper paperEntity : listPaperEntity) {
			PaperDTO map = mapper.map(paperEntity, PaperDTO.class);
			listPaperDto.add(map);
		}
		
		result.put("Message", "Read All Paper Success");
		result.put("Data", listPaperDto);
		result.put("totalItem", listPaperDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{id}")
	public  Map<String, Object> getPaperByIdDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Paper paperEntity = paperRepository.findUserById(id);
		
		PaperDTO map = mapper.map(paperEntity, PaperDTO.class);
		
		result.put("Status", "Success");
		result.put("Message", "Read Paper by Id Success");
		result.put("Data", map);
		
		return result;
	}
	
	@Transactional
	@PutMapping("/updateDto/{id}")
	public  Map<String, Object> updatePaperDto(@RequestBody Paper paper, @PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = paperRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Paper", "id", id));
		
		paperEntity = mapper.map(paper, Paper.class);
		paperEntity.setPaperId(id);
		paperRepository.updatePaper(paperEntity.getQualityName(), paperEntity.getPaperPrice(), id);
		
		result.put("message", "Update Paper Success");
		result.put("data", paper);
		
		return result;
	}
	
	@Transactional
	@DeleteMapping("/deleteDto/{id}")
	public  Map<String, Object> deletePaperDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = paperRepository.findUserById(id);
		
		PaperDTO map = mapper.map(paperEntity, PaperDTO.class);
		paperRepository.deleteById(id);
		
		result.put("message", "Delete Paper Success");
		result.put("data", map);
		
		return result;
	}
}
