package com.example.BookProjectNewTaofik.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.ReviewerDTO;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.models.Reviewer;
import com.example.BookProjectNewTaofik.repositories.ReviewerRepository;

@RestController
@RequestMapping("/api/reviewer")
public class ReviewerController {

	@Autowired
	ReviewerRepository reviewerRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<Reviewer> getAllReviewers() {
        return reviewerRepository.findAll();
    }
	
	@PostMapping("/create")
	public Reviewer createReviewer(@RequestBody Reviewer reviewer){
		return reviewerRepository.save(reviewer);
	}
	
	@PutMapping("/update/{id}")
	public Reviewer updateReviewer(@PathVariable(value = "id") Long reviewerId,
            @Valid @RequestBody Reviewer reviewerDetails) {

		Reviewer reviewer = reviewerRepository.findById(reviewerId)
		 	.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "id", reviewerId));

		reviewer.setReviewerName(reviewerDetails.getReviewerName());
		reviewer.setCountry(reviewerDetails.getCountry());
		reviewer.setVerified(reviewerDetails.isVerified());

		Reviewer updatedReviewer = reviewerRepository.save(reviewer);
		return updatedReviewer;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteReviewer(@PathVariable(value = "id") Long reviewerId) {
       Reviewer reviewer = reviewerRepository.findById(reviewerId)
                .orElseThrow(() -> new ResourceNotFoundException("Reviewer", "id", reviewerId));

       reviewerRepository.delete(reviewer);

       return ResponseEntity.ok().build();
	}
	 
	@GetMapping("/get/{id}")
	public Reviewer getReviewerById(@PathVariable(value = "id") Long reviewerId) {
        return reviewerRepository.findById(reviewerId)
                .orElseThrow(() -> new ResourceNotFoundException("Reviewer", "id", reviewerId));
    }
	
	@Transactional
	@PostMapping("/createDto")
	public Map<String, Object> createReviewerDto(@RequestBody Reviewer reviewer) {
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewerEntity = mapper.map(reviewer, Reviewer.class);
		reviewerRepository.insertReviewer(reviewerEntity.getReviewerName(), reviewerEntity.getCountry(), reviewerEntity.isVerified());
		reviewer.setReviewerId(reviewerEntity.getReviewerId());
		result.put("Data", reviewer);
		return result;
		
	}
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findReviewersDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Reviewer> listReviewerEntity = reviewerRepository.findAllReviewer();
		List<ReviewerDTO> listReviewerDto = new ArrayList<ReviewerDTO>();
		
		for(Reviewer reviewerEntity : listReviewerEntity) {
			ReviewerDTO map = mapper.map(reviewerEntity, ReviewerDTO.class);
			listReviewerDto.add(map);
		}
		
		result.put("Message", "Read All Reviewer Success");
		result.put("Data", listReviewerDto);
		result.put("totalItem", listReviewerDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{id}")
	public  Map<String, Object> getReviewerByIdDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Reviewer reviewerEntity = reviewerRepository.findReviewerById(id);
		
		ReviewerDTO map = mapper.map(reviewerEntity, ReviewerDTO.class);
		
		result.put("Status", "Success");
		result.put("Message", "Read Reviewer by Id Success");
		result.put("Data", map);
		
		return result;
	}
	
	@Transactional
	@PutMapping("/updateDto/{id}")
	public  Map<String, Object> updateReviewerDto(@RequestBody Reviewer reviewer, @PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Reviewer reviewerEntity = reviewerRepository.findReviewerById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "id", id));
		
		reviewerEntity = mapper.map(reviewer, Reviewer.class);
		reviewerEntity.setReviewerId(id);
		reviewerRepository.updateReviewer(reviewerEntity.getReviewerName(), reviewerEntity.getCountry(), reviewerEntity.isVerified(), id);
		
		result.put("message", "Update Reviewer Success");
		result.put("data", reviewer);
		
		return result;
	}
	
	@Transactional
	@DeleteMapping("/deleteDto/{id}")
	public  Map<String, Object> deleteReviewerDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Reviewer reviewerEntity = reviewerRepository.findReviewerById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Reviewer", "id", id));
		
		ReviewerDTO map = mapper.map(reviewerEntity, ReviewerDTO.class);
		reviewerRepository.deleteUserById(id);
		
		result.put("message", "Delete Reviewer Success");
		result.put("data", map);
		
		return result;
	}
}
