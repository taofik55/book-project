package com.example.BookProjectNewTaofik.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.OrderBookDetailsDTO;
import com.example.BookProjectNewTaofik.enums.FinalPrice;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.models.Book;
import com.example.BookProjectNewTaofik.models.OrderBookDetails;
import com.example.BookProjectNewTaofik.models.OrderDetailsKey;
import com.example.BookProjectNewTaofik.repositories.BookRepository;
import com.example.BookProjectNewTaofik.repositories.OrderBookDetailsRepository;

@RestController
@RequestMapping("/api/orderdetails")
public class OrderBookDetailsController implements FinalPrice {
	@Autowired
	OrderBookDetailsRepository orderBookDetailsRepository;
	
	@Autowired
	BookRepository bookRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<OrderBookDetails> getAllOrderBookDetailss() {
        return orderBookDetailsRepository.findAll();
    }
	
	@PostMapping("/create")
	public OrderBookDetails createOrderBookDetails(@RequestBody OrderBookDetails orderBookDetails){
		return orderBookDetailsRepository.save(orderBookDetails);
	}
	
	@PutMapping("/update/{id}")
	public OrderBookDetails updateOrderBookDetails(@PathVariable(value = "id") OrderDetailsKey orderBookDetailsId,
            @Valid @RequestBody OrderBookDetails orderBookDetailsDetails) {

		OrderBookDetails orderBookDetails = orderBookDetailsRepository.findById(orderBookDetailsId)
		 	.orElseThrow(() -> new ResourceNotFoundException("OrderBookDetails", "id", orderBookDetailsId));

		orderBookDetails.setQuantity(orderBookDetailsDetails.getQuantity());
		orderBookDetails.setDiscount(orderBookDetailsDetails.getDiscount());
		orderBookDetails.setTax(orderBookDetailsDetails.getTax());
		orderBookDetails.setBook(orderBookDetailsDetails.getBook());
		orderBookDetails.setOrder(orderBookDetailsDetails.getOrder());

		OrderBookDetails updatedOrderBookDetails = orderBookDetailsRepository.save(orderBookDetails);
		return updatedOrderBookDetails;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteOrderBookDetails(@PathVariable(value = "id") OrderDetailsKey orderBookDetailsId) {
       OrderBookDetails orderBookDetails = orderBookDetailsRepository.findById(orderBookDetailsId)
                .orElseThrow(() -> new ResourceNotFoundException("OrderBookDetails", "id", orderBookDetailsId));

       orderBookDetailsRepository.delete(orderBookDetails);

       return ResponseEntity.ok().build();
	}
	 
	@GetMapping("/get/{id}")
	public OrderBookDetails getOrderBookDetailsById(@PathVariable(value = "id") OrderDetailsKey orderBookDetailsId) {
        return orderBookDetailsRepository.findById(orderBookDetailsId)
                .orElseThrow(() -> new ResourceNotFoundException("OrderBookDetails", "id", orderBookDetailsId));
    }
	
	@Transactional
	@PostMapping("/createDto")
	public Map<String, Object> createOrderBookDetailsDto(@RequestBody OrderBookDetails orderBookDetails) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		OrderBookDetails orderBookDetailsEntity = mapper.map(orderBookDetails, OrderBookDetails.class);
		orderBookDetailsRepository.insertOrderDetails(orderBookDetailsEntity.getId().getBookId(), orderBookDetailsEntity.getId().getOrderId(), orderBookDetailsEntity.getQuantity(), orderBookDetailsEntity.getDiscount(), orderBookDetailsEntity.getTax());
		
		orderBookDetails.setId(orderBookDetailsEntity.getId());
		result.put("Data", orderBookDetails);
		return result;
		
	}
	
	private BigDecimal calculateTax(OrderBookDetails orderBookDetails, Book bookEntity) {
		BigDecimal tax = new BigDecimal(0);
		
		double qty = taxRate * orderBookDetails.getQuantity();
		
		tax = tax.add(bookEntity.getPrice().multiply(new BigDecimal(qty)));
		return tax;
	}
	
	private BigDecimal validateDiscount(OrderBookDetails orderBookDetails) {
		Set<OrderBookDetails> temp = orderBookDetails.getOrder().getOrderDetails();
		BigDecimal bookType = BigDecimal.valueOf(temp.size());
		BigDecimal discount = new BigDecimal(0);
		if(bookType.intValue() >= 3) {
			return calculateDiscount(orderBookDetails);
		} else {
			return discount;
		}
	}
	
	private BigDecimal calculateDiscount(OrderBookDetails orderBookDetails) {
		BigDecimal bookPrice = orderBookDetails.getBook().getPrice();
		BigDecimal qty = BigDecimal.valueOf(orderBookDetails.getQuantity());
		return (discountRate.multiply(bookPrice)).multiply(qty);
	}
	
	@PostMapping("/calculateTaxDiscount")
	public Map<String, Object> calculateTaxDiscount() {
		Map<String, Object> result = new HashMap<String, Object>();
		
		List<OrderBookDetails> listOrderDetails = orderBookDetailsRepository.findAll();
		
		for (OrderBookDetails orderBookDetailsEntity : listOrderDetails) {
			orderBookDetailsEntity.setTax(calculateTax(orderBookDetailsEntity, orderBookDetailsEntity.getBook()));
			orderBookDetailsEntity.setDiscount(validateDiscount(orderBookDetailsEntity));
			orderBookDetailsRepository.save(orderBookDetailsEntity);
		}
		
		result.put("Message", "Calculate All Taxes Success");
    	
    	return result;
	}
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findOrderBookDetailssDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<OrderBookDetails> listOrderBookDetailsEntity = orderBookDetailsRepository.findOrderDetails();
		List<OrderBookDetailsDTO> listOrderBookDetailsDto = new ArrayList<OrderBookDetailsDTO>();
		
		for(OrderBookDetails orderBookDetailsEntity : listOrderBookDetailsEntity) {
			OrderBookDetailsDTO map = mapper.map(orderBookDetailsEntity, OrderBookDetailsDTO.class);
			listOrderBookDetailsDto.add(map);
		}
		
		result.put("Message", "Read All OrderBookDetails Success");
		result.put("Data", listOrderBookDetailsDto);
		result.put("totalItem", listOrderBookDetailsDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{bookid}/{orderid}")
    public Map<String, Object> getOrderBookByIdMapper(@PathVariable(value = "bookid") Long bookId, @PathVariable(value = "orderid") Long orderId){
        Map<String, Object> result = new HashMap<String, Object>();
        OrderDetailsKey id = new OrderDetailsKey(bookId, orderId);
        OrderBookDetails orderBookEntity = orderBookDetailsRepository.findOrderDetailsId(id);
        OrderBookDetailsDTO orderBookDto = mapper.map(orderBookEntity, OrderBookDetailsDTO.class);
        result.put("Message", "Read Id Success");
        result.put("Data", orderBookDto);

        return result;
    }
	
	@Transactional
	@PutMapping("/updateDto/{bookid}/{orderid}")
	public  Map<String, Object> updateOrderBookDetailsDto(@RequestBody OrderBookDetails orderBookDetails, @PathVariable(value = "bookid") Long bookId, @PathVariable(value = "orderid") Long orderId) {
		Map<String, Object> result = new HashMap<String, Object>();
		OrderDetailsKey id = new OrderDetailsKey(bookId, orderId);
		OrderBookDetails orderBookDetailsEntity = orderBookDetailsRepository.findOrderDetailsId(id);
				//.orElseThrow(() -> new ResourceNotFoundException("OrderBookDetails", "id", id));
		
		orderBookDetailsEntity = mapper.map(orderBookDetails, OrderBookDetails.class);
		orderBookDetailsEntity.setId(id);
		orderBookDetailsRepository.save(orderBookDetailsEntity);
		
		result.put("message", "Update OrderBookDetails Success");
		result.put("data", orderBookDetails);
		
		return result;
	}
	
	@Transactional
	@DeleteMapping("/deleteDto/{bookid}/{orderid}")
	public  Map<String, Object> deleteOrderBookDetailsDto(@PathVariable(value = "bookid") Long bookId, @PathVariable(value = "orderid") Long orderId) {
		Map<String, Object> result = new HashMap<String, Object>();
		OrderDetailsKey id = new OrderDetailsKey(bookId, orderId);
		OrderBookDetails orderBookDetailsEntity = orderBookDetailsRepository.findOrderDetailsId(id);
				//.orElseThrow(() -> new ResourceNotFoundException("OrderBookDetails", "id", id));
		
		OrderBookDetailsDTO map = mapper.map(orderBookDetailsEntity, OrderBookDetailsDTO.class);
		orderBookDetailsRepository.deleteOrderDetails(id);
		
		result.put("message", "Delete OrderBookDetails Success");
		result.put("data", map);
		
		return result;
	}
}
