package com.example.BookProjectNewTaofik.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.BookRatingDTO;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.models.BookRating;
import com.example.BookProjectNewTaofik.repositories.BookRatingRepository;

@RestController
@RequestMapping("/api/bookrating")
public class BookRatingController {
	
	@Autowired
	BookRatingRepository bookRatingRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<BookRating> getAllBookRatings() {
        return bookRatingRepository.findAll();
    }
	
	@PostMapping("/create")
	public BookRating createBookRating(@RequestBody BookRating bookRating){
		return bookRatingRepository.save(bookRating);
	}
	
	@PutMapping("/update/{id}")
	public BookRating updateBookRating(@PathVariable(value = "id") Long bookRatingId,
            @Valid @RequestBody BookRating bookRatingDetails) {

		BookRating bookRating = bookRatingRepository.findById(bookRatingId)
		 	.orElseThrow(() -> new ResourceNotFoundException("BookRating", "id", bookRatingId));

		bookRating.setRatingName(bookRatingDetails.getRatingName());
		bookRating.setRatingRate(bookRatingDetails.getRatingRate());
		bookRating.setRatingScore(bookRatingDetails.getRatingScore());
		bookRating.setBook(bookRatingDetails.getBook());
		bookRating.setReviewer(bookRatingDetails.getReviewer());

		BookRating updatedBookRating = bookRatingRepository.save(bookRating);
		return updatedBookRating;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteBookRating(@PathVariable(value = "id") Long bookRatingId) {
       BookRating bookRating = bookRatingRepository.findById(bookRatingId)
                .orElseThrow(() -> new ResourceNotFoundException("BookRating", "id", bookRatingId));

       bookRatingRepository.delete(bookRating);

       return ResponseEntity.ok().build();
	}
	 
	@GetMapping("/get/{id}")
	public BookRating getBookRatingById(@PathVariable(value = "id") Long bookRatingId) {
        return bookRatingRepository.findById(bookRatingId)
                .orElseThrow(() -> new ResourceNotFoundException("BookRating", "id", bookRatingId));
    }
	
	@Transactional
	@PostMapping("/createDto")
	public Map<String, Object> createBookRatingDto(@RequestBody BookRating bookRating) {
		Map<String, Object> result = new HashMap<String, Object>();
		BookRating bookRatingEntity = mapper.map(bookRating, BookRating.class);
		bookRatingRepository.insertBookRating(bookRatingEntity.getRatingName(), bookRatingEntity.getRatingRate(), bookRatingEntity.getRatingScore(), bookRatingEntity.getBook(), bookRatingEntity.getReviewer());
		bookRating.setBookRatingId(bookRatingEntity.getBookRatingId());
		result.put("Data", bookRating);
		return result;
		
	}
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findBookRatingsDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<BookRating> listBookRatingEntity = bookRatingRepository.findAllBookRating();
		List<BookRatingDTO> listBookRatingDto = new ArrayList<BookRatingDTO>();
		
		for(BookRating bookRatingEntity : listBookRatingEntity) {
			BookRatingDTO map = mapper.map(bookRatingEntity, BookRatingDTO.class);
			listBookRatingDto.add(map);
		}
		
		result.put("Message", "Read All BookRating Success");
		result.put("Data", listBookRatingDto);
		result.put("totalItem", listBookRatingDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{id}")
	public  Map<String, Object> getBookRatingByIdDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		BookRating bookRatingEntity = bookRatingRepository.findUserById(id);
		
		BookRatingDTO map = mapper.map(bookRatingEntity, BookRatingDTO.class);
		
		result.put("Status", "Success");
		result.put("Message", "Read BookRating by Id Success");
		result.put("Data", map);
		
		return result;
	}
	
	@Transactional
	@PutMapping("/updateDto/{id}")
	public  Map<String, Object> updateBookRatingDto(@RequestBody BookRating bookRating, @PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		BookRating bookRatingEntity = bookRatingRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("BookRating", "id", id));
		
		bookRatingEntity = mapper.map(bookRating, BookRating.class);
		bookRatingEntity.setBookRatingId(id);
		bookRatingRepository.updateBookRating(bookRatingEntity.getRatingName(), bookRatingEntity.getRatingRate(), bookRatingEntity.getRatingScore(), bookRatingEntity.getBook(), bookRatingEntity.getReviewer(), id);
		
		result.put("message", "Update BookRating Success");
		result.put("data", bookRating);
		
		return result;
	}
	
	@Transactional
	@DeleteMapping("/deleteDto/{id}")
	public  Map<String, Object> deleteBookRatingDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		BookRating bookRatingEntity = bookRatingRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("BookRating", "id", id));
		
		BookRatingDTO map = mapper.map(bookRatingEntity, BookRatingDTO.class);
		bookRatingRepository.deleteUserById(id);
		
		result.put("message", "Delete BookRating Success");
		result.put("data", map);
		
		return result;
	}
}
