package com.example.BookProjectNewTaofik.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.OrderDTO;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.models.Order;
import com.example.BookProjectNewTaofik.models.OrderBookDetails;
import com.example.BookProjectNewTaofik.repositories.OrderBookDetailsRepository;
import com.example.BookProjectNewTaofik.repositories.OrderRepository;

@RestController
@RequestMapping("/api/order")
public class OrderController {
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	OrderBookDetailsRepository orderBookDetailsRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<Order> getAllOrders() {
        return orderRepository.findAll();
    }
	
	@PostMapping("/create")
	public Order createOrder(@RequestBody Order order){
		return orderRepository.save(order);
	}
	
	@PutMapping("/update/{id}")
	public Order updateOrder(@PathVariable(value = "id") Long orderId,
            @Valid @RequestBody Order orderDetails) {

		Order order = orderRepository.findById(orderId)
		 	.orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));

		order.setOrderDate(orderDetails.getOrderDate());
		order.setTotalOrder(orderDetails.getTotalOrder());
		order.setCustomer(orderDetails.getCustomer());
		
		Order updatedOrder = orderRepository.save(order);
		return updatedOrder;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteOrder(@PathVariable(value = "id") Long orderId) {
       Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));

       orderRepository.delete(order);

       return ResponseEntity.ok().build();
	}
	 
	@GetMapping("/get/{id}")
	public Order getOrderById(@PathVariable(value = "id") Long orderId) {
        return orderRepository.findById(orderId)
                .orElseThrow(() -> new ResourceNotFoundException("Order", "id", orderId));
    }
	
	@Transactional
	@PostMapping("/createDto")
	public Map<String, Object> createOrderDto(@RequestBody Order order) {
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = mapper.map(order, Order.class);
		orderRepository.insertOrder(orderEntity.getOrderDate(), orderEntity.getTotalOrder(), orderEntity.getCustomer());
		order.setOrderId(orderEntity.getOrderId());
		result.put("Data", order);
		return result;
		
	}
	
	//HITUNG HASIL TOTAL
	public BigDecimal calculateTotalOrder(Order order) {
        BigDecimal totalOrder = new BigDecimal(0);
        for (OrderBookDetails orderDetailEntity : order.getOrderDetails()) {
        totalOrder = totalOrder.add((orderDetailEntity.getBook().getPrice().multiply(
        			new BigDecimal(orderDetailEntity.getQuantity()))).add(
        					orderDetailEntity.getTax()).subtract(
        							orderDetailEntity.getDiscount()));
        }
        return totalOrder;
    }
	
	@PostMapping("/calculateTotalOrder")
	private Map<String, Object> updateAll(){
        Map<String, Object> result = new HashMap<String, Object>();
        List<Order> listOrder = orderRepository.findAll();

        for (Order orderEntity : listOrder) {
            orderEntity.setTotalOrder(calculateTotalOrder(orderEntity));
            orderRepository.save(orderEntity);
        }

        result.put("Message", "Success Update All Total");
        return result;
    }
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findOrdersDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Order> listOrderEntity = orderRepository.findAllOrder();
		List<OrderDTO> listOrderDto = new ArrayList<OrderDTO>();
		
		for(Order orderEntity : listOrderEntity) {
			OrderDTO map = mapper.map(orderEntity, OrderDTO.class);
			listOrderDto.add(map);
		}
		
		result.put("Message", "Read All Order Success");
		result.put("Data", listOrderDto);
		result.put("totalItem", listOrderDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{id}")
	public  Map<String, Object> getOrderByIdDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Order orderEntity = orderRepository.findUserById(id);
		
		OrderDTO map = mapper.map(orderEntity, OrderDTO.class);
		
		result.put("Status", "Success");
		result.put("Message", "Read Order by Id Success");
		result.put("Data", map);
		
		return result;
	}
	
	@Transactional
	@PutMapping("/updateDto/{id}")
	public  Map<String, Object> updateOrderDto(@RequestBody Order order, @PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Order orderEntity = orderRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Order", "id", id));
		
		orderEntity = mapper.map(order, Order.class);
		orderEntity.setOrderId(id);
		orderRepository.updateOrder(orderEntity.getOrderDate(), orderEntity.getTotalOrder(), orderEntity.getCustomer(), id);
		
		result.put("message", "Update Order Success");
		result.put("data", order);
		
		return result;
	}
	
	@Transactional
	@DeleteMapping("/deleteDto/{id}")
	public  Map<String, Object> deleteOrderDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Order orderEntity = orderRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Order", "id", id));
		
		OrderDTO map = mapper.map(orderEntity, OrderDTO.class);
		orderRepository.deleteUserById(id);
		
		result.put("message", "Delete Order Success");
		result.put("data", map);
		
		return result;
	}
}
