package com.example.BookProjectNewTaofik.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.DTO.CustomerDTO;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.models.Customer;
import com.example.BookProjectNewTaofik.repositories.CustomerRepository;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {
	@Autowired
	CustomerRepository customerRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }
	
	@PostMapping("/create")
	public Customer createCustomer(@RequestBody Customer customer){
		return customerRepository.save(customer);
	}
	
	@PutMapping("/update/{id}")
	public Customer updateCustomer(@PathVariable(value = "id") Long customerId,
            @Valid @RequestBody Customer customerDetails) {

		Customer customer = customerRepository.findById(customerId)
		 	.orElseThrow(() -> new ResourceNotFoundException("Customer", "id", customerId));

		customer.setCustomerName(customerDetails.getCustomerName());
		customer.setCountry(customerDetails.getCountry());
		customer.setAddress(customerDetails.getAddress());
		customer.setPhoneNumber(customerDetails.getPhoneNumber());
		customer.setPostalCode(customerDetails.getPostalCode());
		customer.setEmail(customerDetails.getEmail());

		Customer updatedCustomer = customerRepository.save(customer);
		return updatedCustomer;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteCustomer(@PathVariable(value = "id") Long customerId) {
       Customer customer = customerRepository.findById(customerId)
                .orElseThrow(() -> new ResourceNotFoundException("Customer", "id", customerId));

       customerRepository.delete(customer);

       return ResponseEntity.ok().build();
	}
	 
	@GetMapping("/get/{id}")
	public Customer getCustomerById(@PathVariable(value = "id") Long customerId) {
        return customerRepository.findById(customerId)
                .orElseThrow(() -> new ResourceNotFoundException("Customer", "id", customerId));
    }
	
	@PostMapping("/createDto")
	public Map<String, Object> createCustomerDto(@RequestBody Customer customer) {
		Map<String, Object> result = new HashMap<String, Object>();
		Customer customerEntity = mapper.map(customer, Customer.class);
		customerRepository.save(customerEntity);
		customer.setCustomerId(customerEntity.getCustomerId());
		result.put("Data", customer);
		return result;
		
	}
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findCustomersDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Customer> listCustomerEntity = customerRepository.findAll();
		List<CustomerDTO> listCustomerDto = new ArrayList<CustomerDTO>();
		
		for(Customer customerEntity : listCustomerEntity) {
			CustomerDTO map = mapper.map(customerEntity, CustomerDTO.class);
			listCustomerDto.add(map);
		}
		
		result.put("Message", "Read All Customer Success");
		result.put("Data", listCustomerDto);
		result.put("totalItem", listCustomerDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{id}")
	public  Map<String, Object> getCustomerByIdDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Customer customerEntity = customerRepository.findById(id).get();
		
		CustomerDTO map = mapper.map(customerEntity, CustomerDTO.class);
		
		result.put("Status", "Success");
		result.put("Message", "Read Customer by Id Success");
		result.put("Data", map);
		
		return result;
	}
	
	@PutMapping("/updateDto/{id}")
	public  Map<String, Object> updateCustomerDto(@RequestBody Customer customer, @PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Customer customerEntity = customerRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customer", "id", id));
		
		customerEntity = mapper.map(customer, Customer.class);
		customerEntity.setCustomerId(id);
		customerRepository.save(customerEntity);
		
		result.put("message", "Update Customer Success");
		result.put("data", customer);
		
		return result;
	}
	
	@DeleteMapping("/deleteDto/{id}")
	public  Map<String, Object> deleteCustomerDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Customer customerEntity = customerRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Customer", "id", id));
		
		CustomerDTO map = mapper.map(customerEntity, CustomerDTO.class);
		customerRepository.delete(customerEntity);
		
		result.put("message", "Delete Customer Success");
		result.put("data", map);
		
		return result;
	}
}
