package com.example.BookProjectNewTaofik.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.models.Book;
import com.example.BookProjectNewTaofik.models.Publisher;
import com.example.BookProjectNewTaofik.DTO.BookDTO;
import com.example.BookProjectNewTaofik.enums.FinalPrice;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.repositories.BookRepository;
import com.example.BookProjectNewTaofik.repositories.PublisherRepository;

@RestController
@RequestMapping("/api/book")
public class BookController implements FinalPrice {
	@Autowired
	BookRepository bookRepository;
	
	@Autowired
    PublisherRepository publisherRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }
	
	@PostMapping("/create")
	public Book createBook(@RequestBody Book book){
		return bookRepository.save(book);
	}
	
	@PutMapping("/update/{id}")
	public Book updateBook(@PathVariable(value = "id") Long bookId,
            @Valid @RequestBody Book bookDetails) {

		Book book = bookRepository.findById(bookId)
		 	.orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));

		book.setPrice(bookDetails.getPrice());
		book.setReleaseDate(bookDetails.getReleaseDate());
		book.setTitle(bookDetails.getTitle());
		book.setAuthor(bookDetails.getAuthor());
		book.setPublisher(bookDetails.getPublisher());

		Book updatedBook = bookRepository.save(book);
		return updatedBook;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteBook(@PathVariable(value = "id") Long bookId) {
       Book book = bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));

       bookRepository.delete(book);

       return ResponseEntity.ok().build();
	}
	
	@Transactional
	@PostMapping("/createDto")
	public Map<String, Object> createBookDto(@RequestBody BookDTO book) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Publisher publisherEntity = publisherRepository.findById(book.getPublisher().getPublisherId()).get();
		
		BigDecimal bookPrice = calculateBookPrice(publisherEntity);
		
		book.setPrice(bookPrice);
		Book bookEntity = mapper.map(book, Book.class);
		bookRepository.insertBook(bookPrice, bookEntity.getReleaseDate(), bookEntity.getTitle(), bookEntity.getAuthor(), bookEntity.getPublisher());
		book.setBookId(bookEntity.getBookId());
		result.put("Message", "Create Book success");
    	result.put("Result", book);
    	
    	return result;
	}
	
	private BigDecimal calculateBookPrice(Publisher publisherEntity) {
		BigDecimal bookPrice = new BigDecimal(0);
		final double keuntungan = costRate; //masukkan interface
		
		bookPrice = bookPrice.add(publisherEntity.getPaper().getPaperPrice().multiply(new BigDecimal(keuntungan)));
		return bookPrice;
	}
	
	@PostMapping("/book/calculateBookPrice")
    public Map<String, Object> calculateAllBookPrice() {
    	Map<String, Object> result = new HashMap<String, Object>();
    	
    	List<Book> listBook = bookRepository.findAll();
    	
    	for (Book bookEntity : listBook) {
			bookEntity.setPrice(calculateBookPrice(bookEntity.getPublisher()));
			
			bookRepository.save(bookEntity);
		}
    	
    	result.put("Message", "Calculate All Book Price Success");
    	
    	return result;
    }
	 
	@GetMapping("/get/{id}")
	public Book getBookById(@PathVariable(value = "id") Long bookId) {
        return bookRepository.findById(bookId)
                .orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
    }
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findBooksDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Book> listBookEntity = bookRepository.findAllBook();
		List<BookDTO> listBookDto = new ArrayList<BookDTO>();
		
		for(Book bookEntity : listBookEntity) {
			BookDTO map = mapper.map(bookEntity, BookDTO.class);
			listBookDto.add(map);
		}
		
		result.put("Message", "Read All Book Success");
		result.put("Data", listBookDto);
		result.put("totalItem", listBookDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{id}")
	public  Map<String, Object> getBookByIdDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Book bookEntity = bookRepository.findBookById(id);
		
		BookDTO map = mapper.map(bookEntity, BookDTO.class);
		
		result.put("Status", "Success");
		result.put("Message", "Read Book by Id Success");
		result.put("Data", map);
		
		return result;
	}
	
	@Transactional
	@PutMapping("/updateDto/{id}")
	public  Map<String, Object> updateBookDto(@RequestBody Book book, @PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Book bookEntity = bookRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
		
		bookEntity = mapper.map(book, Book.class);
		bookEntity.setBookId(id);
		bookRepository.updateBook(bookEntity.getTitle(), bookEntity.getReleaseDate(), bookEntity.getAuthor(), bookEntity.getPublisher(), bookEntity.getPrice(), id);
		book.setBookId(bookEntity.getBookId());
		result.put("message", "Update Book Success");
		result.put("data", book);
		
		return result;
	}
	
	@Transactional
	@DeleteMapping("/deleteDto/{id}")
	public  Map<String, Object> deleteBookDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Book bookEntity = bookRepository.findBookById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Book", "id", id));
		
		BookDTO map = mapper.map(bookEntity, BookDTO.class);
		bookRepository.deleteBookById(id);
		
		result.put("message", "Delete Book Success");
		result.put("data", map);
		
		return result;
	}
}