package com.example.BookProjectNewTaofik.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.models.Author;
import com.example.BookProjectNewTaofik.DTO.AuthorDTO;
import com.example.BookProjectNewTaofik.exceptions.ResourceNotFoundException;
import com.example.BookProjectNewTaofik.repositories.AuthorRepository;

@RestController
@RequestMapping("/api/author")
public class AuthorController {
	@Autowired
	AuthorRepository authorRepository;
	
	ModelMapper mapper = new ModelMapper();
	
	@GetMapping("/readAll")
    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }
	
	@PostMapping("/create")
	public Author createAuthor(@RequestBody Author author){
		return authorRepository.save(author);
	}
	
	@PutMapping("/update/{id}")
	public Author updateAuthor(@PathVariable(value = "id") Long authorId,
            @Valid @RequestBody Author authorDetails) {

		Author author = authorRepository.findById(authorId)
		 	.orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));

		author.setFirstName(authorDetails.getFirstName());
		author.setLastName(authorDetails.getLastName());
		author.setGender(authorDetails.getGender());
		author.setAge(authorDetails.getAge());
		author.setCountry(authorDetails.getCountry());
		author.setRating(authorDetails.getRating());

		Author updatedAuthor = authorRepository.save(author);
		return updatedAuthor;
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<?> deleteAuthor(@PathVariable(value = "id") Long authorId) {
       Author author = authorRepository.findById(authorId)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));

       authorRepository.delete(author);

       return ResponseEntity.ok().build();
	}
	 
	@GetMapping("/get/{id}")
	public Author getAuthorById(@PathVariable(value = "id") Long authorId) {
        return authorRepository.findById(authorId)
                .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));
    }
	
	@Transactional
	@PostMapping("/createDto")
	public Map<String, Object> createAuthorDto(@RequestBody Author author) {
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorEntity = mapper.map(author, Author.class);
		authorRepository.insertAuthor(authorEntity.getAge(), authorEntity.getCountry(), authorEntity.getCountry(), authorEntity.getGender(), authorEntity.getLastName(), authorEntity.getRating());
		author.setAuthorId(authorEntity.getAuthorId());
		result.put("Data", author);
		return result;
		
	}
	
	@GetMapping("/readAllDto")
	public  Map<String, Object> findAuthorsDto() {
		Map<String, Object> result = new HashMap<String, Object>();
		List<Author> listAuthorEntity = authorRepository.findAllAuthor();
		List<AuthorDTO> listAuthorDto = new ArrayList<AuthorDTO>();
		
		for(Author authorEntity : listAuthorEntity) {
			AuthorDTO map = mapper.map(authorEntity, AuthorDTO.class);
			listAuthorDto.add(map);
		}
		
		result.put("Message", "Read All Author Success");
		result.put("Data", listAuthorDto);
		result.put("totalItem", listAuthorDto.size());
		
		return result;
	}
	
	@GetMapping("/getDto/{id}")
	public  Map<String, Object> getAuthorByIdDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		Author authorEntity = authorRepository.findUserById(id);
		
		AuthorDTO map = mapper.map(authorEntity, AuthorDTO.class);
		
		result.put("Status", "Success");
		result.put("Message", "Read Author by Id Success");
		result.put("Data", map);
		
		return result;
	}
	
	@Transactional
	@PutMapping("/updateDto/{id}")
	public  Map<String, Object> updateAuthorDto(@RequestBody Author author, @PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Author authorEntity = authorRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));
		
		authorEntity = mapper.map(author, Author.class);
		authorEntity.setAuthorId(id);
		authorRepository.updateAuthor(authorEntity.getAge(), authorEntity.getCountry(), authorEntity.getCountry(), authorEntity.getGender(), authorEntity.getLastName(), authorEntity.getRating(), id);
		author.setAuthorId(authorEntity.getAuthorId());
		
		result.put("message", "Update Author Success");
		result.put("data", author);
		
		return result;
	}
	
	@Transactional
	@DeleteMapping("/deleteDto/{id}")
	public  Map<String, Object> deleteAuthorDto(@PathVariable(value = "id") Long id) {
		Map<String, Object> result = new HashMap<String, Object>();
		
		Author authorEntity = authorRepository.findUserById(id);
				//.orElseThrow(() -> new ResourceNotFoundException("Author", "id", id));
		
		AuthorDTO map = mapper.map(authorEntity, AuthorDTO.class);
		authorRepository.deleteUserById(id);
		
		result.put("message", "Delete Author Success");
		result.put("data", map);
		
		return result;
	}
}