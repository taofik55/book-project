package com.example.BookProjectNewTaofik.views.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.BookProjectNewTaofik.views.VmPaperPublisher;
import com.example.BookProjectNewTaofik.views.DTO.VmPaperPublisherDTO;
import com.io.iona.springboot.controllers.HibernateViewController;

@RestController
@RequestMapping("/api/viewpaperpublisher")
public class VmPaperPublisherController extends HibernateViewController<VmPaperPublisher, VmPaperPublisherDTO> {

}
