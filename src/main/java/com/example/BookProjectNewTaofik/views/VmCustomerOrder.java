package com.example.BookProjectNewTaofik.views;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Immutable
@Table(name = "vm_customer_order", schema = "public")
public class VmCustomerOrder {
	private Long no;
	private String customerName;
	private BigDecimal totalOrder;
	
	
	@Column(name = "customer_name")
	public String getCustomerName() {
		return customerName;
	}
	
	@Id
	@Column(name = "no", unique = true, nullable = false)
	public Long getNo() {
		return no;
	}
	public void setNo(Long no) {
		this.no = no;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	@Column(name = "total_order")
	public BigDecimal getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}
	
}
