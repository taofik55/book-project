package com.example.BookProjectNewTaofik.views.DTO;

public class VmPaperPublisherDTO {
	private String qualityName;
	private String companyName;
	public String getQualityName() {
		return qualityName;
	}
	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
}
