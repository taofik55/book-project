package com.example.BookProjectNewTaofik.views.DTO;

import java.math.BigDecimal;

public class VmCustomerOrderDTO {
	private Long no;
	public Long getNo() {
		return no;
	}
	public void setNo(Long no) {
		this.no = no;
	}
	private String customerName;
	private BigDecimal totalOrder;
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public BigDecimal getTotalOrder() {
		return totalOrder;
	}
	public void setTotalOrder(BigDecimal totalOrder) {
		this.totalOrder = totalOrder;
	}
	
}
